var barProgess = 0;
var functionValidateForm = null;
var TGP_validateForm = null;

const TGP_removeClassBtn = () => {
  const button = document.querySelector(".generatePdfBtn button");
  button.classList.remove("disabledNext");
};

const TGP_addClassBtn = () => {
  const button = document.querySelector(".generatePdfBtn button");
  button.classList.add("disabledNext");
};

const TGP_loadBarProgress = () => {
  const DIVbarProgess = document.createElement("div");
  DIVbarProgess.setAttribute("class", "barProgess");
  document.body.appendChild(DIVbarProgess);
  DIVbarProgess.innerHTML = `<div class="gradient"></div>`;
};
window.addEventListener("load", TGP_loadBarProgress);

const TGP_hiddenQuestion2 = (questing) => {
  var styleHiddenQuestion = document.getElementById("styleHiddenQuestion");
  if (!styleHiddenQuestion) {
    styleHiddenQuestion = document.createElement("style");
    styleHiddenQuestion.setAttribute("id", "styleHiddenQuestion");
    document.body.appendChild(styleHiddenQuestion);
  }

  const newStyle = `
        [data-field]:not([data-field="Submit"]):not([data-field="${questing}"]){
            display:none;
        }
    `;
  styleHiddenQuestion.innerHTML = newStyle;
};

const TGP_hiddenQuestion = (next, pre) => {
  if (pre == null) {
    // no es default
    TGP_removeClassBtn();
  } else {
    const elementAcual = document.querySelector(
      `[name="${pre}"][value="Default"]:checked`
    );
    if (elementAcual == null) {
      // no es default
      TGP_removeClassBtn();
    } else {
      // si es default
      TGP_addClassBtn();
    }
  }

  const siguiente = document.querySelector(".generatePdfBtn button");
  siguiente.onclick = (e) => {
    e.preventDefault();
    TGP_addClassBtn();
    if (!siguiente.classList.value.includes("disabledBtn")) {
      TGP_PrintViewPdf();
    } else {
      TGP_hiddenQuestion2(next);
    }
  };
  const back = document.querySelector("#previu a");
  if (pre == null) {
    back.style.display = "none";
  } else {
    back.style.display = "inline-block";
    back.onclick = () => {
      TGP_hiddenQuestion2(pre);
      var elementPre = document.querySelector(
        `[data-field="${pre}"] input[value="Default"]`
      );
      if (elementPre) {
        elementPre.click();
      } else {
        elementPre = document.querySelector(`[data-field="${pre}"] select`);
        elementPre.value = "Select";
      }
      const data = TGP_validateForm();
      TGP_printTemplateAndDisableBtn(data);
      document.body.style.setProperty("--barProgess", barProgess);
      TGP_addClassBtn();
    };
  }
};

const TGP_printTemplateAndDisableBtn = (data, loadViewPDF = true) => {
  if (loadViewPDF) {
    const view_pdf = document.getElementById(`view-pdf`);
    view_pdf.innerHTML = data.disableBtn
      ? TGP_getLoaderSkeleton()
      : data.template();
  }
  const btnForm = document.querySelector(`.generatePdfBtn button`);

  const isValidGeneratePDF = data.disableBtn
    ? false
    : TGP_validateRequireForPrintPdf(data);
  btnForm.classList[isValidGeneratePDF ? "remove" : "add"]("disabledBtn");
};
const TGP_validateRequireForPrintPdf = (data) => {
  const requireForPrintPdf = document.querySelectorAll(".requireForPrintPdf");
  for (let i = 0; i < requireForPrintPdf.length; i++) {
    const element = requireForPrintPdf[i];
    if (element.value == "") {
      element.classList.add("error");
      if (!element.placeholder.includes(` (Required *)`)) {
        element.placeholder = element.placeholder + ` (Required *)`;
      }
      element.onchange = () => {
        TGP_printTemplateAndDisableBtn(data, false);
      };
      return false;
    }
    element.classList.remove("error");
  }
  return true;
};

const TGP_loadFunctionForm = (statusForm) => {
  const insputs = document.querySelectorAll(`#form input,#form select`);
  for (var i = 0; i < insputs.length; i++) {
    insputs[i].addEventListener("change", (e) => {
      const id = e.target.name;
      const value = e.target.value;
      statusForm[id] = value;
      const data = TGP_validateForm();
      TGP_printTemplateAndDisableBtn(data);
      document.body.style.setProperty("--barProgess", barProgess);
    });
  }
  const back = document.querySelector("#previu a");
  back.style.display = "none";
  const siguiente = document.querySelector(".generatePdfBtn button");
  siguiente.classList.add("disabledBtn");
  siguiente.onclick = (e) => {
    e.preventDefault();
  };
  const view_pdf = document.getElementById(`view-pdf`);
  view_pdf.innerHTML = TGP_getLoaderSkeleton();
};
const TGP_PrintViewPdf = () => {
  if (confirm("Are you sure of your answers?")) {
    const view_pdf = document.getElementById(`view-pdf`);
    const view_pdfPrint = document.createElement("div");
    view_pdfPrint.setAttribute("class", "view-pdf view-pdfPrint");
    view_pdfPrint.setAttribute("id", "view-pdf-print");

    document.body.appendChild(view_pdfPrint);
    view_pdfPrint.innerHTML = view_pdf.innerHTML;
    const inputs = document.querySelectorAll(
      "#view-pdf input,#view-pdf textarea"
    );
    const inputsPrint = document.querySelectorAll(
      "#view-pdf-print input,#view-pdf-print textarea"
    );
    for (let i = 0; i < inputs.length; i++) {
      inputsPrint[i].value = inputs[i].value;
    }
    view_pdf.outerHTML = "";
    document.body.classList.add("printPdf");

    // Ocultar el elemento con la clase "content-int"
    const contentInt = document.querySelector(".content-int");
    if (contentInt) {
      contentInt.classList.add("hidden");
      // También puedes usar contentInt.classList.add('display-none');
    }

    window.onafterprint = function (e) {
      console.log(e);
      window.location.href = "https://amazonappeal.io/thank-you/";
    };
    print();
  }
};
