const TGP_statusForm2 = {
  "Have you sent any appeals to Amazon yet?": "Default",
  "What is the current Status of your suspended ASIN/Account?": "Select",
  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?":
    "Default",
  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?":
    "Default",
  "Have you sent any escalation letters yet?": "Default",
  "Where did you send the escalation letter?": "Default",
  "Have you sent any escalation letters yet?": "Default",
  "Where did you send the escalation letter?": "Default",
};

const TGP_validateForm2 = () => {
  var disableBtn = true;
  var template = TGP_Template.template1;

  if (
    TGP_statusForm2["Have you sent any appeals to Amazon yet?"] == "Default"
  ) {
    TGP_hiddenQuestion("Have you sent any appeals to Amazon yet?", null);
  }

  if (TGP_statusForm2["Have you sent any appeals to Amazon yet?"] == "No") {
    disableBtn = false;
  }

  if (TGP_statusForm2["Have you sent any appeals to Amazon yet?"] == "Yes") {
    TGP_hiddenQuestion(
      "What is the current Status of your suspended ASIN/Account?",
      "Have you sent any appeals to Amazon yet?"
    );
    barProgess = 50;
    if (
      TGP_statusForm2[
        "What is the current Status of your suspended ASIN/Account?"
      ] == "#1Amazon is asking for more information."
    ) {
      TGP_hiddenQuestion(
        "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
        "What is the current Status of your suspended ASIN/Account?"
      );
      barProgess = 70;
      if (
        TGP_statusForm2[
          "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "0"
      ) {
        disableBtn = false;
        barProgess = 0;
      }
      if (
        TGP_statusForm2[
          "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "1/3"
      ) {
        disableBtn = false;
        barProgess = 0;
      }
      if (
        TGP_statusForm2[
          "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "4+"
      ) {
        template = TGP_Template.template2;
        TGP_hiddenQuestion(
          "Have you sent any escalation letters yet?",
          "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        );
        barProgess = 80;
        if (
          TGP_statusForm2["Have you sent any escalation letters yet?"] == "No"
        ) {
          disableBtn = false;
          barProgess = 0;
        }
        if (
          TGP_statusForm2["Have you sent any escalation letters yet?"] == "Yes"
        ) {
          TGP_hiddenQuestion(
            "Where did you send the escalation letter?",
            "Have you sent any escalation letters yet?"
          );
          barProgess = 90;
          if (
            TGP_statusForm2["Where did you send the escalation letter?"] !=
            "Default"
          ) {
            disableBtn = false;
            barProgess = 0;
          }
        }
      }
    } else if (
      TGP_statusForm2[
        "What is the current Status of your suspended ASIN/Account?"
      ] == "#2Amazon is not responding."
    ) {
      TGP_hiddenQuestion(
        "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
        "What is the current Status of your suspended ASIN/Account?"
      );
      barProgess = 70;
      if (
        TGP_statusForm2[
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "0"
      ) {
        disableBtn = false;
        barProgess = 0;
      }
      if (
        TGP_statusForm2[
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "1"
      ) {
        disableBtn = false;
        barProgess = 0;
      }
      if (
        TGP_statusForm2[
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "2+"
      ) {
        template = TGP_Template.template2;
        TGP_hiddenQuestion(
          "Have you sent any escalation letters yet?",
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        );
        barProgess = 80;
        if (
          TGP_statusForm2["Have you sent any escalation letters yet?"] == "No"
        ) {
          disableBtn = false;
          barProgess = 0;
        }
        if (
          TGP_statusForm2["Have you sent any escalation letters yet?"] == "Yes"
        ) {
          TGP_hiddenQuestion(
            "Where did you send the escalation letter?",
            "Have you sent any escalation letters yet?"
          );
          barProgess = 90;
          if (
            TGP_statusForm2["Where did you send the escalation letter?"] !=
            "Default"
          ) {
            disableBtn = false;
            barProgess = 0;
          }
        }
      }
    } else if (
      TGP_statusForm2[
        "What is the current Status of your suspended ASIN/Account?"
      ] == "#3 Amazon told me their decision is final."
    ) {
      TGP_hiddenQuestion(
        "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
        "What is the current Status of your suspended ASIN/Account?"
      );
      barProgess = 70;
      if (
        TGP_statusForm2[
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "0"
      ) {
        disableBtn = false;
        barProgess = 0;
      }
      if (
        TGP_statusForm2[
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "1"
      ) {
        disableBtn = false;
        barProgess = 0;
      }
      if (
        TGP_statusForm2[
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        ] == "2+"
      ) {
        template = TGP_Template.template2;
        TGP_hiddenQuestion(
          "Have you sent any escalation letters yet?",
          "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
        );
        barProgess = 80;
        if (
          TGP_statusForm2["Have you sent any escalation letters yet?"] == "No"
        ) {
          disableBtn = false;
          barProgess = 0;
        }
        if (
          TGP_statusForm2["Have you sent any escalation letters yet?"] == "Yes"
        ) {
          TGP_hiddenQuestion(
            "Where did you send the escalation letter?",
            "Have you sent any escalation letters yet?"
          );
          barProgess = 90;
          if (
            TGP_statusForm2["Where did you send the escalation letter?"] !=
            "Default"
          ) {
            disableBtn = false;
            barProgess = 0;
          }
        }
      }
    }
  }

  return {
    disableBtn,
    template,
  };
};

const TGP_loadForm2 = () => {
  nameFormUse = "Linked Account Suspensions";
  TGP_loadContent("Linked Account Suspensions");
  TGP_validateForm = TGP_validateForm2;
  TGP_loadFunctionForm(TGP_statusForm2);
  functionValidateForm = TGP_validateForm2;
  TGP_hiddenQuestion2("Have you sent any appeals to Amazon yet?");
};
