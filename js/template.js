var nameFormUse = "";
const TGP_Title = {
  title: [
    "Amazon Seller Performance,",
    "Dear Amazon Seller Performance Team,",
    "Amazon Seller Performance Team, ",
    "To the Amazon Seller Performance Team,",
    "Dear Amazon,",
    "To whom it may concern,",
  ],
  title2: [
    "Root Cause",
    "The Root Cause",
    "The Root Cause of the issue  ",
    "The Root Cause of the Problem",
  ],
  title3: [
    "Actions to resolve",
    "Actions to Resolve the Issue",
    "Actions to Resolve the Problem",
    "Actions We Have taken to Resolve the Issue",
    "Actions We have taken to Resolve the Problem",
    "Actions to fix",
  ],
  title4: [
    "Actions to prevent",
    "Actions to prevent the Issue in the Future",
    "Actions We Have Taken to prevent the Issue from occurring in the future",
    "Actions to avoid",
  ],
  title5: [
    "Dear Amazon,",
    "Dear Jeff,",
    "Dear Mr. Bezos,",
    "Mr. Bezos,",
    "Dear Amazon,",
    "To Whom It May Concern:",
  ],
};
const TGP_Text = {
  text: [
    [
      "Thank you for taking the opportunity to review our account.",
      "Thank you for reviewing our account.",
      "We appreciate that you are reviewing our account.",
      "Thank you for taking the time to review our account.",
      "We appreciate that you are taking the time to review our account ",
    ],
    [
      `Our seller account has been suspended due to ${nameFormUse}`,
      `Our Amazon seller account has been suspended due to ${nameFormUse}`,
      `Our seller account has been temporarily suspended due to ${nameFormUse} `,
      `Our Amazon seller account has been temporarily suspended because of ${nameFormUse}`,
      `Below, we have provided the information that you have requested concerning the ${nameFormUse} `,
      `Below, we have included the information that you have requested concerning the ${nameFormUse} `,
      `In this letter, we have provided the information that you have requested concerning the ${nameFormUse} `,
      `In this letter, we have included the information that you have requested concerning the ${nameFormUse} `,
      `We have included the information that you have requested concerning the ${nameFormUse} `,
    ],
    [
      "We have written a plan of action, which will detail the root cause of the problem, as well as the steps we have taken to fix and prevent the issue from occurring in the future.",
      "We will provide our plan of action, which will explain the root cause of the issue, and will detail all of the actions we have taken to fix and prevent the issue from occurring in the future.",
    ],
  ],

  text2: [
    [
      "We have personally conducted a thorough review of our sales process, and",
      "We have personally conducted a thorough review of our Amazon business process, and",
      "We have personally executed a comprehensive review of our Amazon business process, and",
      "We have conducted an extensive review of our sales process, and",
      "We have performed an extensive review of our sales process, and",
      "We have executed an extensive review of our sales process, and",
      "After undergoing an extensive review of our sales process,",
      "After personally conducting an extensive review of our sales process,",
      "After personally conducting an extensive review of our business process,",
      "After conducting an extensive review of our processes,",
      "We have taken a significant amount of time trying to understand this issue, and",
      "After thoroughly reviewing our case,",
      "We have taken a significant amount of time trying to understand the root cause of this issue, and",
    ],
    [
      "we have determined that the issue that directly caused the “inauthentic item complaints” was that",
      "we have determined that the issue that caused the “inauthentic item complaints” was that",
      "we have concluded that the issue was caused by",
      "we have found that the issue was caused by",
      "we have found that the issue was caused due to",
    ],
  ],
  text3: [
    [
      "Thank you for reviewing my case.",
      "Thank you for taking the time to review my case.",
      "Thank you for reviewing my situation. ",
      "Thank you for taking the time to review my situation. ",
      "Thank you for taking the time to look at my case. ",
      "I appreciate you taking the time to look at my case.",
      "I really appreciate you taking the time to look at my case. ",
      "I appreciate that you still take the time to review the cases of sellers.",
    ],
    [
      "The purpose of this email is to raise an issue regarding s the review process of our suspended account. ",
      "This letter’s purpose is to raise an issue regarding the review process of our suspended Amazon account by your seller support staff.",
    ],
    [`Our account has been suspended due to ${nameFormUse}. `],
    [
      "We have had multiple interactions with seller support staff, and we have provided multiple plans of action,",
      "We have provided multiple appeals to the Seller Performance Team,",
      "We have appealed our suspension multiple times to the Seller Performance Team, ",
      "We have made multiple attempts to appeal our suspension through Seller Support, ",
      "To date, we have sent multiple appeals to the Seller Performance Team,",
    ],
    [
      "but despite our best efforts to provide satisfactory information, they have been unresponsive to our appeals.",
      "but they have been unresponsive to our appeals.",
      "but they have been unresponsive to our explanations.",
      "but they do not seem to be taking the information into consideration.",
    ],
    [
      "We greatly value the ability to be able to sell our products on Amazon, and we are proud of the quality products that our company provides to our customers. ",
      "We take our responsibility to the customers of Amazon very seriously, and we greatly value the opportunity to use the Amazon platform for our business.",
    ],
    [
      "We would like to kindly request to have someone properly review the details of our case so we could receive a fair and timely resolution to this issue. ",
      "All we are requesting is to have someone properly review the details of our case. ",
      "We would greatly appreciate if someone would properly review the details of our case. ",
      "We would like to have someone accurately review the specifics of our case so we can have a fair resolution to our issue. ",
    ],
  ],
  text4: [
    [
      "My account has been suspended because it has been linked to multiple seller accounts. This must",
      "Be an error, because this is my only account.",
      "My account has been suspended because it has been linked to another suspended seller account.",
      "This cannot be the case, because this is the only Amazon seller account I have ever owned.",
    ],
    [
      "after reviewing your policies concerning related accounts, I would like to clarify a few things:",
      "i have spent some time reviewing Amazon’s related account policies, and I would like to clarify",
      "a few things:",
    ],
    [
      "I am the only owner of this account.",
      "I am the sole owner of this Amazon seller account.",
      "I do not share ownership with this seller account at all.",
    ],
    [
      "This is my only Amazon Account.",
      "This is my only seller account.",
      "This is the only Amazon seller account that I own.",
    ],
    [
      "I have never had another Amazon account in the past.",
      "I have never owned another Amazon Seller account before.",
    ],
    [
      "However, you may have believed that my account was related because: ",
      "Some possible explanations for my account being linked are:",
      "Here are some possible explanations for why my account has been linked:",
    ],
    [
      "I have friends who sell on Amazon, I occasionally access my account from their computers.",
      "In the past, I have allowed my friends to access their Amazon seller accounts from my computer.",
    ],
    [
      "I use VPN services in order to protect my data. I try to not use it when I log in to my amazon",
      "seller account, but sometimes I forget to shut it off.",
      "I occasionally use a VPN service to access my Amazon Seller Account ",
    ],
    [
      "I have used public WIFI to access my account when travelling. ",
      "Whenever I am out of town, I use public Wi-Fi to access my Amazon Seller account.",
    ],
    [
      "I have used 3rd parties to manage certain aspects of my business operations.",
      "I use 3rd party software to manage my Amazon business.",
      "In the past, I have used 3rd parties to manage certain aspects of my business.",
    ],
  ],
  text5: [
    [
      "We have immediately removed the ASIN/ASINs in question.",
      "We immediately removed the ASIN/ASINs that caused the issue.",
      "We immediately removed the offending ASIN/ASINs.",
      "We immediately removed the listings that caused the issue.",
      "I have written a plan of action, which will detail the root cause of the problem, as well as the steps ,I have taken to fix and prevent the issue from occurring in the future. ",
    ],
    [
      "We contacted the rights owner, and we have apologized for selling their products without their permission.",
      "We have immediately emailed the rights owner, and we have apologized for selling their products without their authorization.",
      "As soon as we received their contact info, we emailed the rights owner, and we thoroughly apologized for selling their products without their express permission.",
      "I have included my plan of action below.",
    ],
    [
      "We have conducted an extensive audit of all our products to ensure that we are fully compliant with all copyright laws and Amazon guidelines with all of the products we sell.",
      "We have immediately emailed the rights owner, and we have apologized for selling their products without their authorization.",
      "We have audited all of the products in our warehouse, and we have confirmed that any products we currently sell are fully compliant with Amazon’s copyright policies.",
      "I will provide my plan of action, which will explain the root cause of the issue, and will detail all of the actions I have taken to fix and prevent the issue from occurring in the future.",
    ],
    [
      "We have consulted an attorney for help to ensure that we have the right procedures in place to prevent IP infringement. ",
      "We consulted an intellectual property attorney to make sure we always follow the right procedures when selling products from another company.",
      "We have spoken with an attorney who is versed in IP law, and he has advised us on how to make sure all our products are fully compliant.",
    ],
    [
      "We have consulted Amazon selling experts to create a process that always ensures that we are compliant with Amazon intellectual property policies.",
      "We have consulted ",
    ],
    [
      "We have implemented a new rigorous vetting process for all of our new products. This will verify that we have selling rights and that they fully comply with Amazon guidelines before we start selling them on Amazon.",
    ],
    [
      "We have downloaded 3rd party programs to help us identify the products that require express permission from the rights owner to sell.",
    ],
    [
      "We have updated, and we will continue to update every three months, our relevant staff training to ensure complete compliance with all up to date Amazon guidelines, especially regarding copyright infringement.",
    ],
  ],
  text6: [
    [
      "We have personally conducted a thorough review of our sales process, and",
      "We have personally conducted a thorough review of our Amazon business process, and",
      "We have personally executed a comprehensive review of our Amazon business process, and",
      "We have conducted an extensive review of our sales process, and",
      "We have performed an extensive review of our sales process, and",
      "We have executed an extensive review of our sales process, and",
      "After undergoing an extensive review of our sales process,",
      "After personally conducting an extensive review of our sales process,",
      "After personally conducting an extensive review of our business process,",
      "After conducting an extensive review of our processes,",
      "We have taken a significant amount of time trying to understand this issue, and",
      "After thoroughly reviewing our case,",
      "We have taken a significant amount of time trying to understand the root cause of this issue, and",
    ],
    [
      "we have determined that the issue that directly caused the “Intellectual Property Suspensions” was that",
      "we have determined that the issue that caused the “Intellectual Property Suspensions” was that",
      "we have concluded that the issue was caused by",
      "we have found that the issue was caused by",
      "we have found that the issue was caused due to",
    ],
  ],
};
var TGP_Content = {
  ...TGP_Title,
  ...TGP_Text,
};

const TGP_loadContent = (nameFormUse) => {
  const TGP_Title = {
    title: [
      "Amazon Seller Performance,",
      "Dear Amazon Seller Performance Team,",
      "Amazon Seller Performance Team, ",
      "To the Amazon Seller Performance Team,",
      "Dear Amazon,",
      "To whom it may concern,",
    ],
    title2: [
      "Root Cause",
      "The Root Cause",
      "The Root Cause of the issue  ",
      "The Root Cause of the Problem",
    ],
    title3: [
      "Actions to resolve",
      "Actions to Resolve the Issue",
      "Actions to Resolve the Problem",
      "Actions We Have taken to Resolve the Issue",
      "Actions We have taken to Resolve the Problem",
      "Actions to fix",
    ],
    title4: [
      "Actions to prevent",
      "Actions to prevent the Issue in the Future",
      "Actions We Have Taken to prevent the Issue from occurring in the future",
      "Actions to avoid",
    ],
    title5: [
      "Dear Amazon,",
      "Dear Jeff,",
      "Dear Mr. Bezos,",
      "Mr. Bezos,",
      "Dear Amazon,",
      "To Whom It May Concern:",
    ],
  };

  const TGP_Text = {
    text: [
      [
        "Thank you for taking the opportunity to review our account.",
        "Thank you for reviewing our account.",
        "We appreciate that you are reviewing our account.",
        "Thank you for taking the time to review our account.",
        "We appreciate that you are taking the time to review our account ",
      ],
      [
        `Our seller account has been suspended due to ${nameFormUse}`,
        `Our Amazon seller account has been suspended due to ${nameFormUse}`,
        `Our seller account has been temporarily suspended due to ${nameFormUse} `,
        `Our Amazon seller account has been temporarily suspended because of ${nameFormUse}`,
        `Below, we have provided the information that you have requested concerning the ${nameFormUse} `,
        `Below, we have included the information that you have requested concerning the ${nameFormUse} `,
        `In this letter, we have provided the information that you have requested concerning the ${nameFormUse} `,
        `In this letter, we have included the information that you have requested concerning the ${nameFormUse} `,
        `We have included the information that you have requested concerning the ${nameFormUse} `,
      ],
      [
        "We have written a plan of action, which will detail the root cause of the problem, as well as the steps we have taken to fix and prevent the issue from occurring in the future.",
        "We will provide our plan of action, which will explain the root cause of the issue, and will detail all of the actions we have taken to fix and prevent the issue from occurring in the future.",
      ],
    ],

    text2: [
      [
        "We have personally conducted a thorough review of our sales process, and",
        "We have personally conducted a thorough review of our Amazon business process, and",
        "We have personally executed a comprehensive review of our Amazon business process, and",
        "We have conducted an extensive review of our sales process, and",
        "We have performed an extensive review of our sales process, and",
        "We have executed an extensive review of our sales process, and",
        "After undergoing an extensive review of our sales process,",
        "After personally conducting an extensive review of our sales process,",
        "After personally conducting an extensive review of our business process,",
        "After conducting an extensive review of our processes,",
        "We have taken a significant amount of time trying to understand this issue, and",
        "After thoroughly reviewing our case,",
        "We have taken a significant amount of time trying to understand the root cause of this issue, and",
      ],
      [
        "we have determined that the issue that directly caused the “inauthentic item complaints” was that",
        "we have determined that the issue that caused the “inauthentic item complaints” was that",
        "we have concluded that the issue was caused by",
        "we have found that the issue was caused by",
        "we have found that the issue was caused due to",
      ],
    ],
    text3: [
      [
        "Thank you for reviewing my case.",
        "Thank you for taking the time to review my case.",
        "Thank you for reviewing my situation. ",
        "Thank you for taking the time to review my situation. ",
        "Thank you for taking the time to look at my case. ",
        "I appreciate you taking the time to look at my case.",
        "I really appreciate you taking the time to look at my case. ",
        "I appreciate that you still take the time to review the cases of sellers.",
      ],
      [
        "The purpose of this email is to raise an issue regarding s the review process of our suspended account. ",
        "This letter’s purpose is to raise an issue regarding the review process of our suspended Amazon account by your seller support staff.",
      ],
      [`Our account has been suspended due to ${nameFormUse}. `],
      [
        "We have had multiple interactions with seller support staff, and we have provided multiple plans of action,",
        "We have provided multiple appeals to the Seller Performance Team,",
        "We have appealed our suspension multiple times to the Seller Performance Team, ",
        "We have made multiple attempts to appeal our suspension through Seller Support, ",
        "To date, we have sent multiple appeals to the Seller Performance Team,",
      ],
      [
        "but despite our best efforts to provide satisfactory information, they have been unresponsive to our appeals.",
        "but they have been unresponsive to our appeals.",
        "but they have been unresponsive to our explanations.",
        "but they do not seem to be taking the information into consideration.",
      ],
      [
        "We greatly value the ability to be able to sell our products on Amazon, and we are proud of the quality products that our company provides to our customers. ",
        "We take our responsibility to the customers of Amazon very seriously, and we greatly value the opportunity to use the Amazon platform for our business.",
      ],
      [
        "We would like to kindly request to have someone properly review the details of our case so we could receive a fair and timely resolution to this issue. ",
        "All we are requesting is to have someone properly review the details of our case. ",
        "We would greatly appreciate if someone would properly review the details of our case. ",
        "We would like to have someone accurately review the specifics of our case so we can have a fair resolution to our issue. ",
      ],
    ],
    text4: [
      [
        "My account has been suspended because it has been linked to multiple seller accounts. This must",
        "Be an error, because this is my only account.",
        "My account has been suspended because it has been linked to another suspended seller account.",
        "This cannot be the case, because this is the only Amazon seller account I have ever owned.",
      ],
      [
        "after reviewing your policies concerning related accounts, I would like to clarify a few things:",
        "i have spent some time reviewing Amazon’s related account policies, and I would like to clarify",
        "a few things:",
      ],
      [
        "I am the only owner of this account.",
        "I am the sole owner of this Amazon seller account.",
        "I do not share ownership with this seller account at all.",
      ],
      [
        "This is my only Amazon Account.",
        "This is my only seller account.",
        "This is the only Amazon seller account that I own.",
      ],
      [
        "I have never had another Amazon account in the past.",
        "I have never owned another Amazon Seller account before.",
      ],
      [
        "However, you may have believed that my account was related because: ",
        "Some possible explanations for my account being linked are:",
        "Here are some possible explanations for why my account has been linked:",
      ],
      [
        "I have friends who sell on Amazon, I occasionally access my account from their computers.",
        "In the past, I have allowed my friends to access their Amazon seller accounts from my computer.",
      ],
      [
        "I use VPN services in order to protect my data. I try to not use it when I log in to my amazon",
        "seller account, but sometimes I forget to shut it off.",
        "I occasionally use a VPN service to access my Amazon Seller Account ",
      ],
      [
        "I have used public WIFI to access my account when travelling. ",
        "Whenever I am out of town, I use public Wi-Fi to access my Amazon Seller account.",
      ],
      [
        "I have used 3rd parties to manage certain aspects of my business operations.",
        "I use 3rd party software to manage my Amazon business.",
        "In the past, I have used 3rd parties to manage certain aspects of my business.",
      ],
    ],
    text5: [
      [
        "We have immediately removed the ASIN/ASINs in question.",
        "We immediately removed the ASIN/ASINs that caused the issue.",
        "We immediately removed the offending ASIN/ASINs.",
        "We immediately removed the listings that caused the issue.",
      ],
      [
        "We contacted the rights owner, and we have apologized for selling their products without their permission.",
        "We have immediately emailed the rights owner, and we have apologized for selling their products without their authorization.",
        "As soon as we received their contact info, we emailed the rights owner, and we thoroughly apologized for selling their products without their express permission.",
      ],
      [
        "We have conducted an extensive audit of all our products to ensure that we are fully compliant with all copyright laws and Amazon guidelines with all of the products we sell.",
        "We have immediately emailed the rights owner, and we have apologized for selling their products without their authorization.",
        "We have audited all of the products in our warehouse, and we have confirmed that any products we currently sell are fully compliant with Amazon’s copyright policies.",
      ],
      [
        "We have consulted an attorney for help to ensure that we have the right procedures in place to prevent IP infringement. ",
        "We consulted an intellectual property attorney to make sure we always follow the right procedures when selling products from another company.",
        "We have spoken with an attorney who is versed in IP law, and he has advised us on how to make sure all our products are fully compliant.",
      ],
      [
        "We have consulted Amazon selling experts to create a process that always ensures that we are compliant with Amazon intellectual property policies.",
        "We have consulted ",
      ],
      [
        "We have implemented a new rigorous vetting process for all of our new products. This will verify that we have selling rights and that they fully comply with Amazon guidelines before we start selling them on Amazon.",
      ],
      [
        "We have downloaded 3rd party programs to help us identify the products that require express permission from the rights owner to sell.",
      ],
      [
        "We have updated, and we will continue to update every three months, our relevant staff training to ensure complete compliance with all up to date Amazon guidelines, especially regarding copyright infringement.",
      ],
    ],
    text6: [
      [
        "We have personally conducted a thorough review of our sales process, and",
        "We have personally conducted a thorough review of our Amazon business process, and",
        "We have personally executed a comprehensive review of our Amazon business process, and",
        "We have conducted an extensive review of our sales process, and",
        "We have performed an extensive review of our sales process, and",
        "We have executed an extensive review of our sales process, and",
        "After undergoing an extensive review of our sales process,",
        "After personally conducting an extensive review of our sales process,",
        "After personally conducting an extensive review of our business process,",
        "After conducting an extensive review of our processes,",
        "We have taken a significant amount of time trying to understand this issue, and",
        "After thoroughly reviewing our case,",
        "We have taken a significant amount of time trying to understand the root cause of this issue, and",
      ],
      [
        "we have determined that the issue that directly caused the “Intellectual Property Suspensions” was that",
        "we have determined that the issue that caused the “Intellectual Property Suspensions” was that",
        "we have concluded that the issue was caused by",
        "we have found that the issue was caused by",
        "we have found that the issue was caused due to",
      ],
    ],
  };
  TGP_Content = {
    ...TGP_Title,
    ...TGP_Text,
  };
};

const TGP_getRandomMsj = (msjs) => {
  return msjs[Math.round(Math.random() * (msjs.length - 1))];
};
const TGP_GenerateTemplate1 = (data) => {
  return `
    <div>
        <div>
            <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>

            <h3 style="font-weight:900">
                ${TGP_getRandomMsj(TGP_Content.title)}
            </h3>
            <p>
                ${TGP_getRandomMsj(TGP_Content.text[0])}
                ${TGP_getRandomMsj(TGP_Content.text[1])}
                ${TGP_getRandomMsj(TGP_Content.text[2])}
            </p>
        </div>
        <div>
            <h3 class="TitleRoot" style="font-weight:900";>
                ${TGP_getRandomMsj(TGP_Content.title2)}
            </h3>
            <p>
                ${TGP_getRandomMsj(TGP_Content.text2[0])}
                ${TGP_getRandomMsj(TGP_Content.text2[1])}
                <textarea type="text" placeholder="And i have concluded that the issue was caused by a severe lack of understanding of Amazon’s polices and procedures.We are new sellers on Amazon, and we did not know that we couldn't sell products from a retail store without the rights owner’s express permission." class="requireForPrintPdf notBorderTextarea"></textarea>
            </p>
            <h3 class="" style="font-weight:900">
                ${TGP_getRandomMsj(TGP_Content.title3)}
            </h3>
            <ul>
                <li>
                    We immediately removed the ASIN/ASINs that caused the issue.
                </li>
                <li>
                    We have immediately emailed the rights owner, and we have apologized for selling their products without
                    their authorization.</li>
                <li style="margin-right:30px;" >
                    <input placeholder="Add any additional actions that may be helpful (optional)" type="text"
                        style="border-bottom:1; width:"450px;" border-top:0; border-left:0; border-right:0; border-radius:0; font-family:'sofia-pro'; class="notBorder">
                </li>
            </ul>
            <h3 class="" style="font-weight:900">
                ${TGP_getRandomMsj(TGP_Content.title4)}
            </h3>
            <ul>
            <li>
            ${TGP_getRandomMsj(TGP_Content.text5[2])}
        </li>
        <li>
            ${TGP_getRandomMsj(TGP_Content.text5[3])}
        </li>
        <li>
            ${TGP_getRandomMsj(TGP_Content.text5[4])}
        </li>
        <li>
            ${TGP_getRandomMsj(TGP_Content.text5[5])}
        </li>
        <li>
            ${TGP_getRandomMsj(TGP_Content.text5[6])}
        </li>
        <li>
            ${TGP_getRandomMsj(TGP_Content.text5[7])}
        </li>
            </ul>
        </div>
    </div>
    `;
};
const TGP_GenerateTemplate2 = (data) => {
  return `
        <div>
            <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>

             <h3 style="display:flex">
                Suggested Email Subject:<span style="font-weight:900;"> Account Review</span>
            </h3>
            <br>

            <h3>
                ${TGP_getRandomMsj(TGP_Content.title5)}
            </h3>
            <p>
                ${TGP_getRandomMsj(TGP_Content.text3[0])}
                ${TGP_getRandomMsj(TGP_Content.text3[1])}
                ${TGP_getRandomMsj(TGP_Content.text3[2])}
                <br>
                <br>
                ${TGP_getRandomMsj(TGP_Content.text3[3])}
                ${TGP_getRandomMsj(TGP_Content.text3[4])}
                <br>
                <br>
                ${TGP_getRandomMsj(TGP_Content.text3[5])}
                ${TGP_getRandomMsj(TGP_Content.text3[6])}
                <br>
                <br>
                Thank you,
            </p>
            <div class="forman" style="border-radius:6px" style="padding-right:20px;padding-left:10px">
                <input style="border-radius:6px" class="yourName requireForPrintPdf notBorder" placeholder="Your name" type="text"/>
                <input style="border-radius:6px" class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
                <input style="border-radius:6px" class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
            </div>
            
        </div>
    `;
};
const TGP_GenerateTemplate3 = (data) => {
  return `
        <div>
        <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
            <h3 >
                ${TGP_getRandomMsj(TGP_Content.title5)}
            </h3>
            <p>
                ${TGP_getRandomMsj(TGP_Content.text4[0])}
                ${TGP_getRandomMsj(TGP_Content.text4[1])}
                <br>
                <ul>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text4[2])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text4[3])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text4[4])}
                    </li>
                </ul>
                <br>
                <h3 style="font-weight:900;">
                 ${TGP_getRandomMsj(TGP_Content.text4[5])}
                </h3>
                
                <br>
                    <ul>
                        <li>
                            ${TGP_getRandomMsj(TGP_Content.text4[6])}
                        </li>
                        <li>
                            ${TGP_getRandomMsj(TGP_Content.text4[7])}
                        </li>
                        <li>
                            ${TGP_getRandomMsj(TGP_Content.text4[8])}
                        </li>
                        <li>
                            ${TGP_getRandomMsj(TGP_Content.text4[9])}
                        </li>
                    </ul>
                <br>
                I hope you find that the above information sufficiently explains that I have not violated your policies, and I would kindly request that my account is reinstated. 
                <br>    
                Thank You,

            </p>
            <input class="yourName requireForPrintPdf notBorder " placeholder="Your name" type="text"/>
            <input class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
            <input class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
        </div>
    `;
};
/**
    Para invalid de paquete 3 template 4
 */

const TGP_GenerateTemplate4 = (data) => {
  return `
        <div>
        <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
            <h3>
                ${TGP_getRandomMsj(TGP_Content.title5)}
            </h3>
            <br>
            <p>
                <br>
                ${TGP_getRandomMsj(TGP_Content.text[0])}
                ${TGP_getRandomMsj(TGP_Content.text[1])}
                <br>
                
                <br>
                ${TGP_getRandomMsj(TGP_Content.text[2])}
                ${TGP_getRandomMsj(TGP_Content.text[3])}
                <br>
                ${TGP_getRandomMsj(TGP_Content.text4[4])}
                <br>
                  
              

            </p>
            <br>
            <h3 class="" stlyle="font-weight:900;">
                -Supplier Information:
            </h3>
            <br>
            <p>
                <input class="yourName requireForPrintPdf notBorder" placeholder="Supplier Name" type="text"/>
                <input class="emailClient requireForPrintPdf notBorder" placeholder="Address" type="text"/>
                <input class="yourContact requireForPrintPdf notBorder" placeholder="Phone Number" type="number"/>
                <input class="yourContact requireForPrintPdf notBorder" placeholder="Website" placeholder="https://example.com" pattern="https://.*" type="url"/>
                <input class="yourContact requireForPrintPdf notBorder" placeholder="Email" type="email"/>
            </p>
            <h3 class="" stlyle="font-weight:900;">
                -Buyer Information:
            </h3>
            <p>
                <input class="yourName requireForPrintPdf notBorder" placeholder="Your Name" type="text"/>
                <input class="emailClient requireForPrintPdf notBorder" placeholder="Address" type="text"/>
                <input class="yourContact requireForPrintPdf notBorder" placeholder="Phone Number" type="number"/>
                <input class="yourContact requireForPrintPdf notBorder" placeholder="Email" type="email"/>
            </p>
            <p>
                -Invoice Date: 
                <input class="yourName requireForPrintPdf notBorder"  type="date"/>
                -Item Description: 
                <input class="yourName requireForPrintPdf notBorder"  type="text"/>
                -Item Quantity: 
                <input type="number requireForPrintPdf notBorder" min="1" max="1000">
            </p>
            <p>
                I sincerely hope that the above, as well as the attached, information satisfactorily addressed the issues at hand, and I would kindly request that my account be reinstated.
                <br>
                Thank you,
            
            </p>
            <input class="yourName requireForPrintPdf notBorder" placeholder="Your name" type="text"/>
            <input class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
            <input class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
            
        </div>
    `;
};
/**
    Para Valid de paquete 3 template 5
 */
const TGP_GenerateTemplate5 = (data) => {
  return `
            <div>
            <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
                <h3>
                    ${TGP_getRandomMsj(TGP_Content.title5)}
                </h3>
                <p>
                    ${TGP_getRandomMsj(TGP_Content.text[0])}
                    ${TGP_getRandomMsj(TGP_Content.text[1])}
                    <br>
                </p>
                <br>
                <h3>
                    ${TGP_getRandomMsj(TGP_Content.title2)}
                <h3/>
                <p style="padding-left:0 ; font-size: 15px;">
                    ${TGP_getRandomMsj(TGP_Content.text6[0])}
                    ${TGP_getRandomMsj(TGP_Content.text6[1])}
                </p>
                <div style="margin-right:20px">
                    <textarea type="text" placeholder="We were inexperienced with Amazon’s policies, the quality of our listings was poor, etc." class="requireForPrintPdf notBorderTextarea"></textarea>
                    <br>
                    <textarea type="text" placeholder="Explain the details of what caused the suspension." class="requireForPrintPdf notBorderTextarea"></textarea>
                </div>
                <br>
                
                <h3 class="" style="font-weight:900">
                    ${TGP_getRandomMsj(TGP_Content.title3)}
                </h3>
                <br>
                <ul>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[0])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[1])}
                    </li>
                    <li style="margin-right:30px;" >
                    <input placeholder="Add any additional actions that may be helpful (optional)" type="text"
                        style="border-bottom:1; width:"450px;" border-top:0; border-left:0; border-right:0; border-radius:0; font-family:'sofia-pro'; class="notBorder">
                    </li>
                </ul>
                <br>
                <h3 class="" style="font-weight:900">
                    ${TGP_getRandomMsj(TGP_Content.title4)}
                </h3>
                <ul>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[2])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[3])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[4])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[5])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[6])}
                    </li>
                    <li>
                        ${TGP_getRandomMsj(TGP_Content.text5[7])}
                    </li>
                </ul>

                
               
                
            </div>
        `;
};
/**
    Valid#1
 */
const TGP_GenerateTemplate6 = (data) => {
  return `
            <div>
            <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
                <p>
                    Dear Sir/Madam,
                
                </p> 
                
                <br>
                <p>
                    Amazon has notified me that they have suspended my seller account due to a copyright complaint from your organization.
                </p> 
                
                <br>
                <p>
                    To this date, I had actually never had a chance to sell anything on Amazon yet. I had purchased your product at a retail store in order to resell it on Amazon, and when I was about to ship the products to FBA I received the notification that my account had been suspended.
                </p>
                
                <br> 
                <p>
                    I want to make it clear that I completely respect your intellectual property rights, and I sincerely apologize for listing your product on Amazon without getting your permission first.
                </p> 
                
                <br>
                <p>
                    
                     The listing has been deleted, and you have my word that I will not attempt to sell any of your products on Amazon without your express permission. 
                </p>
                
                <br>
                <p> 
                    I would really like to get my seller account reinstated so I can try to sell a few other products on Amazon, and I would really appreciate it if you could take a moment to retract your copyright claim by emailing a retraction to notice-retraction@amazon.com.  
                </p>
                
                <br>
                <p>
                    Again, I apologize for infringing on your IP. It only happened because of my lack of experience with selling on Amazon, but I promise to learn from this experience to become a better Amazon seller in the future. 
                </p>
                
                <br>
                <p>
                    If you would kindly notify me of your decision, I would greatly appreciate that as well.
                </p>
                
                <br>
                <p>
                    If you have any questions or concerns, please don’t hesitate to contact me.
                <p>
                
                <br>
                <p>
                    All the best,
                </p>
                
                <div class="forman" style="border-radius:6px" style="padding-right:20px;padding-left:10px">
                    <input style="border-radius:6px" class="yourName requireForPrintPdf notBorder" placeholder="Your name" type="text"/>
                    <input style="border-radius:6px" class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
                    <input style="border-radius:6px" class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
                </div>
            
            </div>
        `;
};

/**
    Valid#2
 */

const TGP_GenerateTemplate7 = (data) => {
  return `
            <div>
            <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
                <p>
                To Whom It May Concern,
                
                </p> 
                
                <br>
                <p>
                This email is concerning the intellectual property claim that you made against my Amazon seller account.
                </p> 
                
                <br>
                <p>
                We are a credible company that has been selling on Amazon, as well as other platforms, for many years. During this time, we have never experienced any intellectual property issues, and we are incredibly proud of the valid products and services that we have provided to our customers over the years. 
                </p>
                
                <br> 
                <p>
                    We only purchase our products from credible wholesalers and distributors, and your product was no different. I will attach our invoice as well as a personal letter from the distributor to this email, and I would encourage you to contact them to verify its authenticity. 
                
                </p> 
                
                <br>
                <p>
                    Due to the outstanding infringement complaint that your company has alleged against us, the entirety of our Amazon business has been put on hold for a significant period of time. This has had a profound negative impact on our company, and we wish nothing more than to move past this. I would kindly request that you retract your infringement complaint and send it to notice-retraction@amazon.com as well as inform us of this occurrence. 
                </p>
                
                <br>
                <p> 
                    Our company takes intellectual property rights very seriously and going forward, we will make sure not to sell any of your company's products without an express letter of authorization from your organization.
                </p>
                
                <br>
                <p>
                    Thank you,
                </p>
                <div class="forman" style="border-radius:6px" style="padding-right:20px;padding-left:10px">
                    <input style="border-radius:6px" class="yourName requireForPrintPdf notBorder" placeholder="Your name" type="text"/>
                    <input style="border-radius:6px" class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
                    <input style="border-radius:6px" class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
                </div>
            
            </div>
        `;
};
/**
    Ivalid#1
*/
const TGP_GenerateTemplate8 = (data) => {
  return `
            <div>
            <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
                <p  style="display:flex;">
                Dear <span><input style="border-bottom: 1px solid #000;padding: 0; border-top: 0;border-radius: 0; margin: 0; border-left: 0; border-right: 0;margin-top: -1px;
                font-size: 16px; padding-left: 10px;" class="requireForPrintPdf notBorder" type="text"/></span> ,

                </p> 
                
                <br>
                <p>
                We are a credible company who has been selling on Amazon, as well as other platforms, for many years. During this time, we have never experienced any intellectual property issues, and we are incredibly proud of the valid products and services that we have provided to our customers over the years. 

                </p> 
                
                <br>
                <p style="display:flex;">
                <label>
                <input type="checkbox" id="cbox1" value="first_checkbox" class="noPrintCheck "> 
                <span>
                <span style ="font-weight:700 !important;">Include if applicable :</span> We purchased your products legitimately, and I have attached the proper documents to this email.
                </span>
                </label>
                     
                </p>
                
                <br> 
                <p>
                     Due to the outstanding infringement complaint that your company has alleged against us, the entirety of our Amazon business has been put on hold for a significant period of time. This has had a profound negative impact on our company, and we wish nothing more than to move past this. I would kindly request that you retract your infringement complaint and send it to notice-retraction@amazon.com as well as inform us of this occurrence.  
                
                </p> 
                <br>
                <p>
                    Thank you,
                </p>
                <div class="forman" style="border-radius:6px" style="padding-right:20px;padding-left:10px">
                    <input requerid style="border-radius:6px" class="yourName requireForPrintPdf notBorder" placeholder="Your name" type="text"/>
                    <input style="border-radius:6px" class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
                    <input style="border-radius:6px" class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
                </div>
            
            </div>
        `;
};

/**
    Invalid#2
 */
const TGP_GenerateTemplate9 = (data) => {
  return `
                <div>
                <span class="content-int">
              <h3 style="font-weight:900">
                Instructions:
              </h3>
              <p>
                If the complaints of inauthentic items are valid, there are a few key things that you need to do in order to get your account reinstated. You have to convincingly convey to Amazon that you made an honest mistake due to lack of experience, you truly understand the mistake you made, and that you have taken actions to make sure that this mistake won't occur in the future.
              </p>
              <h3 style="font-weight:900">
                Alternate Instruction for stage 2:
              </h3>
              <p>
                If the previous appeal letter was not successful at reinstating your account, it is important to review your “Root Cause” section that you provided to Amazon. Make sure to reword it in a way that would show Amazon that you have put significant amounts of effort into understanding what went wrong. 
              </p>
            </span>
                    <p  style="display:flex;">
                        Dear <span><input style="border-bottom: 1px solid #000;padding: 0; border-top: 0;border-radius: 0; margin: 0; border-left: 0; border-right: 0;margin-top: -1px;
                        font-size: 16px; padding-left: 10px;" type="text" class="requireForPrintPdf notBorder" /></span> ,

                    </p> 
                    <br>
                    <p>
                    It has been some time since I last sent you an email, and I am disappointed that we have not received any response.  
                    </p> 
                    
                    <br>
                    <p>
                        We are a credible company who has been selling on Amazon, as well as other platforms, for many years. During this time, we have never experienced any intellectual property issues, and we are incredibly proud of the valid products and services that we have provided to our customers over the years. 

                    </p>
                    
                    <br> 
                    <p style="display:flex;">

                    <label>
                        <input type="checkbox" id="cbox1" value="first_checkbox" class="noPrintCheck"> 
                        <span>
                        We purchased your products legitimately, and I have attached the proper documents to this email.
                        </span>
                    </label>

                    <br>
                    
                        
                    
                    </p> 
                    <br>
                    <p>
                        Due to the outstanding infringement complaint that your company has alleged against us, the entirety of our Amazon business has been put on hold for a significant period of time. This has had a profound negative impact on our company, and we wish nothing more than to move past this. I would kindly request that you retract your infringement complaint and send it to notice-retraction@amazon.com as well as inform us of this occurrence.
                    </p>
                    <br>
                    <p>
                        If your retraction fails to materialize within a reasonable amount of time, we will be left with no other choice but to take legal action. I sincerely hope that it does not have to reach this point, but our Amazon business has been inactive for far too long, and you have caused deliberate financial damage against us. 

                    </p>
                    <br>
                    <p>
                        Thank you,
                    </p>
                    <div class="forman" style="border-radius:6px" style="padding-right:20px;padding-left:10px">
                        <input style="border-radius:6px" class="yourName requireForPrintPdf notBorder" placeholder="Your name" type="text"/>
                        <input style="border-radius:6px" class="emailClient requireForPrintPdf notBorder" placeholder="Email" type="email"/>
                        <input style="border-radius:6px" class="yourContact requireForPrintPdf notBorder" placeholder="Your contact" type="text"/>
                    </div>
                
                </div>
            `;
};

const TGP_Template = {
  template1: TGP_GenerateTemplate1,
  template2: TGP_GenerateTemplate2,
  template3: TGP_GenerateTemplate3,
  template4: TGP_GenerateTemplate4,
  template5: TGP_GenerateTemplate5,
  template6: TGP_GenerateTemplate6,
  template7: TGP_GenerateTemplate7,
  template8: TGP_GenerateTemplate8,
  template9: TGP_GenerateTemplate9,
};
