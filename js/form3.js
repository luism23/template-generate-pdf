const TGP_statusForm3 = {
  "Has your content been removed under DMCA?": "Default",
  "Has Amazon asked you to contact the rights owner?": "Default",
  "Have you sent a retraction letter to the rights owner yet?": "Default",
  "Were the Intellectual property complaints valid or invalid?2": "Default",
  "How many retraction letters from Amazon Appeal Pro have you sent?":
    "Default",
  "Have you sent any appeals to Amazon yet?": "Default",
  "What is the current Status of your suspended ASIN/Account?": "Select",
  "Were the intellectual property complaints valid or invalid?": "Default",
  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?":
    "Default",
  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?":
    "Default",
  "Have you sent any escalation letters yet?": "Default",
  "Where did you send the escalation letter?": "Default",
  "Have you sent any escalation letters yet?": "Default",
  "Where did you send the escalation letter?": "Default",
  "Has Amazon requested a POA along with proof of your product's authenticity?":
    "Default",
};

const TGP_validateForm3 = () => {
  var disableBtn = true;
  var template = TGP_Template.template1;

  if (
    TGP_statusForm3["Has your content been removed under DMCA?"] == "Default"
  ) {
    TGP_hiddenQuestion("Has your content been removed under DMCA?", null);
  }

  if (TGP_statusForm3["Has your content been removed under DMCA?"] == "Yes") {
    TGP_hiddenQuestion("Has your content been removed under DMCA?", null);
    disableBtn = false;
  }
  if (TGP_statusForm3["Has your content been removed under DMCA?"] == "No") {
    TGP_hiddenQuestion(
      "Has Amazon asked you to contact the rights owner?",
      "Has your content been removed under DMCA?"
    );

    barProgess = 20;
    if (
      TGP_statusForm3["Has Amazon asked you to contact the rights owner?"] ==
      "No"
    ) {
      TGP_hiddenQuestion(
        "Have you sent any appeals to Amazon yet?",
        "Has Amazon asked you to contact the rights owner?"
      );

      barProgess = 50;
      if (TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "No") {
        template = TGP_Template.template1;
        TGP_hiddenQuestion(
          "Were the intellectual property complaints valid or invalid?",
          "Have you sent any appeals to Amazon yet?"
        );

        barProgess = 70;
        if (
          TGP_statusForm3[
            "Were the intellectual property complaints valid or invalid?"
          ] == "Valid"
        ) {
          disableBtn = false;
          barProgess = 0;
        } else if (
          TGP_statusForm3[
            "Were the intellectual property complaints valid or invalid?"
          ] == "Invalid"
        ) {
          disableBtn = false;
          barProgess = 0;
        }
      }
      if (
        TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "Yes"
      ) {
        TGP_hiddenQuestion(
          "What is the current Status of your suspended ASIN/Account?",
          "Have you sent any appeals to Amazon yet?"
        );

        barProgess = 70;
        if (
          TGP_statusForm3[
            "What is the current Status of your suspended ASIN/Account?"
          ] == "#1Amazon is asking for more information."
        ) {
          TGP_hiddenQuestion(
            "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
            "What is the current Status of your suspended ASIN/Account?"
          );

          barProgess = 80;
          if (
            TGP_statusForm3[
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "0"
          ) {
            TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );
            barProgess = 90;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] == "Valid"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] == "Invalid"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
          }
          if (
            TGP_statusForm3[
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "1/3"
          ) {
            TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );
            barProgess = 90;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] == "Valid"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] == "Invalid"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
          }
          if (
            TGP_statusForm3[
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "4+"
          ) {
            template = TGP_Template.template2;
            TGP_hiddenQuestion(
              "Have you sent any escalation letters yet?",
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );
            barProgess = 90;
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "No"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "Yes"
            ) {
              TGP_hiddenQuestion(
                "Where did you send the escalation letter?",
                "Have you sent any escalation letters yet?"
              );
              barProgess = 95;
              if (
                TGP_statusForm3["Where did you send the escalation letter?"] !=
                "Default"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
          }
          if (
            TGP_statusForm3[
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "Default"
          ) {
          }
        } else if (
          TGP_statusForm3[
            "What is the current Status of your suspended ASIN/Account?"
          ] == "#2Amazon is not responding."
        ) {
          TGP_hiddenQuestion(
            "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
            "What is the current Status of your suspended ASIN/Account?"
          );

          barProgess = 80;
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "0"
          ) {
            TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );

            barProgess = 90;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] != "Default"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
          }
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "1"
          ) {
            TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );

            barProgess = 90;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] != "Default"
            ) {
              disableBtn = false;
            }
          }
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "2+"
          ) {
            template = TGP_Template.template2;
            TGP_hiddenQuestion(
              "Have you sent any escalation letters yet?",
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );

            barProgess = 90;
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "No"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "Yes"
            ) {
              TGP_hiddenQuestion(
                "Where did you send the escalation letter?",
                "Have you sent any escalation letters yet?"
              );

              barProgess = 95;
              if (
                TGP_statusForm3["Where did you send the escalation letter?"] !=
                "Default"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
          }
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "Default"
          ) {
          }
        } else if (
          TGP_statusForm3[
            "What is the current Status of your suspended ASIN/Account?"
          ] == "#3 Amazon told me their decision is final."
        ) {
          TGP_hiddenQuestion(
            "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
            "What is the current Status of your suspended ASIN/Account?"
          );

          barProgess = 80;
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "0"
          ) {
            TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );

            barProgess = 90;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] != "Default"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
          }
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "1"
          ) {
            TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );

            barProgess = 90;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] != "Default"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
          }
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "2+"
          ) {
            template = TGP_Template.template2;
            TGP_hiddenQuestion(
              "Have you sent any escalation letters yet?",
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            );

            barProgess = 90;
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "No"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "Yes"
            ) {
              TGP_hiddenQuestion(
                "Where did you send the escalation letter?",
                "Have you sent any escalation letters yet?"
              );

              barProgess = 95;
              if (
                TGP_statusForm3["Where did you send the escalation letter?"] !=
                "Default"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
            if (
              TGP_statusForm3["Have you sent any escalation letters yet?"] ==
              "Default"
            ) {
            }
          }
          if (
            TGP_statusForm3[
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
            ] == "Default"
          ) {
          }
        }
        if (
          TGP_statusForm3[
            "What is the current Status of your suspended ASIN/Account?"
          ] == "Select"
        ) {
        }
      }
      if (
        TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "Default"
      ) {
      }
    }
    if (
      TGP_statusForm3["Has Amazon asked you to contact the rights owner?"] ==
      "Yes"
    ) {
      TGP_hiddenQuestion(
        "Have you sent a retraction letter to the rights owner yet?",
        "Has Amazon asked you to contact the rights owner?"
      );

      barProgess = 50;
      if (
        TGP_statusForm3[
          "Have you sent a retraction letter to the rights owner yet?"
        ] == "Yes"
      ) {
        TGP_hiddenQuestion(
          "How many retraction letters from Amazon Appeal Pro have you sent?",
          "Have you sent a retraction letter to the rights owner yet?"
        );

        barProgess = 60;
        if (
          TGP_statusForm3[
            "How many retraction letters from Amazon Appeal Pro have you sent?"
          ] == "2"
        ) {
          TGP_hiddenQuestion(
            "Have you sent any appeals to Amazon yet?",
            "How many retraction letters from Amazon Appeal Pro have you sent?"
          );

          barProgess = 70;
          if (
            TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "No"
          ) {
            template = TGP_Template.template1;
            TGP_hiddenQuestion(
              "Were the intellectual property complaints valid or invalid?",
              "Have you sent any appeals to Amazon yet?"
            );

            barProgess = 70;
            if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] == "Valid"
            ) {
              disableBtn = false;
              barProgess = 0;
            } else if (
              TGP_statusForm3[
                "Were the intellectual property complaints valid or invalid?"
              ] == "Invalid"
            ) {
              disableBtn = false;
              barProgess = 0;
            }
          }
          if (
            TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "Yes"
          ) {
            TGP_hiddenQuestion(
              "What is the current Status of your suspended ASIN/Account?",
              "Have you sent any appeals to Amazon yet?"
            );

            barProgess = 70;
            if (
              TGP_statusForm3[
                "What is the current Status of your suspended ASIN/Account?"
              ] == "#1Amazon is asking for more information."
            ) {
              TGP_hiddenQuestion(
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
                "What is the current Status of your suspended ASIN/Account?"
              );

              barProgess = 80;
              if (
                TGP_statusForm3[
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "0"
              ) {
                TGP_Template.template1;
                TGP_hiddenQuestion(
                  "Were the intellectual property complaints valid or invalid?",
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );
                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] == "Valid"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] == "Invalid"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
              if (
                TGP_statusForm3[
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "1/3"
              ) {
                TGP_Template.template1;
                TGP_hiddenQuestion(
                  "Were the intellectual property complaints valid or invalid?",
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );
                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] == "Valid"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] == "Invalid"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
              if (
                TGP_statusForm3[
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "4+"
              ) {
                template = TGP_Template.template2;
                TGP_hiddenQuestion(
                  "Have you sent any escalation letters yet?",
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );
                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "No"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "Yes"
                ) {
                  TGP_hiddenQuestion(
                    "Where did you send the escalation letter?",
                    "Have you sent any escalation letters yet?"
                  );
                  barProgess = 95;
                  if (
                    TGP_statusForm3[
                      "Where did you send the escalation letter?"
                    ] != "Default"
                  ) {
                    disableBtn = false;
                    barProgess = 0;
                  }
                }
              }
              if (
                TGP_statusForm3[
                  "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "Default"
              ) {
              }
            } else if (
              TGP_statusForm3[
                "What is the current Status of your suspended ASIN/Account?"
              ] == "#2Amazon is not responding."
            ) {
              TGP_hiddenQuestion(
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
                "What is the current Status of your suspended ASIN/Account?"
              );

              barProgess = 80;
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "0"
              ) {
                TGP_Template.template1;
                TGP_hiddenQuestion(
                  "Were the intellectual property complaints valid or invalid?",
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );

                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "1"
              ) {
                TGP_Template.template1;
                TGP_hiddenQuestion(
                  "Were the intellectual property complaints valid or invalid?",
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );

                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                }
              }
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "2+"
              ) {
                template = TGP_Template.template2;
                TGP_hiddenQuestion(
                  "Have you sent any escalation letters yet?",
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );

                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "No"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "Yes"
                ) {
                  TGP_hiddenQuestion(
                    "Where did you send the escalation letter?",
                    "Have you sent any escalation letters yet?"
                  );

                  barProgess = 95;
                  if (
                    TGP_statusForm3[
                      "Where did you send the escalation letter?"
                    ] != "Default"
                  ) {
                    disableBtn = false;
                    barProgess = 0;
                  }
                }
              }
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "Default"
              ) {
              }
            } else if (
              TGP_statusForm3[
                "What is the current Status of your suspended ASIN/Account?"
              ] == "#3 Amazon told me their decision is final."
            ) {
              TGP_hiddenQuestion(
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
                "What is the current Status of your suspended ASIN/Account?"
              );

              barProgess = 80;
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "0"
              ) {
                TGP_Template.template1;
                TGP_hiddenQuestion(
                  "Were the intellectual property complaints valid or invalid?",
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );

                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "1"
              ) {
                TGP_Template.template1;
                TGP_hiddenQuestion(
                  "Were the intellectual property complaints valid or invalid?",
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );

                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Were the intellectual property complaints valid or invalid?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "2+"
              ) {
                template = TGP_Template.template2;
                TGP_hiddenQuestion(
                  "Have you sent any escalation letters yet?",
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                );

                barProgess = 90;
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "No"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "Yes"
                ) {
                  TGP_hiddenQuestion(
                    "Where did you send the escalation letter?",
                    "Have you sent any escalation letters yet?"
                  );

                  barProgess = 95;
                  if (
                    TGP_statusForm3[
                      "Where did you send the escalation letter?"
                    ] != "Default"
                  ) {
                    disableBtn = false;
                    barProgess = 0;
                  }
                }
                if (
                  TGP_statusForm3[
                    "Have you sent any escalation letters yet?"
                  ] == "Default"
                ) {
                }
              }
              if (
                TGP_statusForm3[
                  "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
                ] == "Default"
              ) {
              }
            }
            if (
              TGP_statusForm3[
                "What is the current Status of your suspended ASIN/Account?"
              ] == "Select"
            ) {
            }
          }
          if (
            TGP_statusForm3["Have you sent any appeals to Amazon yet?"] ==
            "Default"
          ) {
          }
        }
        if (
          TGP_statusForm3[
            "How many retraction letters from Amazon Appeal Pro have you sent?"
          ] == "1"
        ) {
          TGP_hiddenQuestion(
            "How many retraction letters from Amazon Appeal Pro have you sent-Valid-Invalid",
            "How many retraction letters from Amazon Appeal Pro have you sent?"
          );

          barProgess = 80;
          template = TGP_Template.template7;
          if (
            TGP_statusForm3[
              "How many retraction letters from Amazon Appeal Pro have you sent-Valid-Invalid"
            ] == "Invalid"
          ) {
            disableBtn = false;
            barProgess = 0;
          } else if (
            TGP_statusForm3[
              "How many retraction letters from Amazon Appeal Pro have you sent-Valid-Invalid"
            ] == "Valid"
          ) {
            template = TGP_Template.template9;
            disableBtn = false;
            barProgess = 0;
          }
        }
        if (
          TGP_statusForm3[
            "How many retraction letters from Amazon Appeal Pro have you sent?"
          ] == "Default"
        ) {
        }
      }
      if (
        TGP_statusForm3[
          "Have you sent a retraction letter to the rights owner yet?"
        ] == "No"
      ) {
        TGP_hiddenQuestion(
          "Were the Intellectual property complaints valid or invalid?2",
          "Have you sent a retraction letter to the rights owner yet?"
        );

        barProgess = 80;
        template = TGP_Template.template6;
        if (
          TGP_statusForm3[
            "Were the Intellectual property complaints valid or invalid?2"
          ] == "Valid"
        ) {
          disableBtn = false;
          barProgess = 0;
        }
        if (
          TGP_statusForm3[
            "Were the Intellectual property complaints valid or invalid?2"
          ] == "Invalid"
        ) {
          template = TGP_Template.template8;
          disableBtn = false;
          barProgess = 0;
        }
      }
      if (
        TGP_statusForm3[
          "Have you sent a retraction letter to the rights owner yet?"
        ] == "NA"
      ) {
        TGP_hiddenQuestion(
          "Have you sent any appeals to Amazon yet?",
          "Have you sent a retraction letter to the rights owner yet?"
        );
        barProgess = 70;
        if (
          TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "No"
        ) {
          template = TGP_Template.template1;
          TGP_hiddenQuestion(
            "Were the intellectual property complaints valid or invalid?",
            "Have you sent any appeals to Amazon yet?"
          );

          barProgess = 70;
          if (
            TGP_statusForm3[
              "Were the intellectual property complaints valid or invalid?"
            ] == "Valid"
          ) {
            disableBtn = false;
            barProgess = 0;
          } else if (
            TGP_statusForm3[
              "Were the intellectual property complaints valid or invalid?"
            ] == "Invalid"
          ) {
            disableBtn = false;
            barProgess = 0;
          }
        }
        if (
          TGP_statusForm3["Have you sent any appeals to Amazon yet?"] == "Yes"
        ) {
          TGP_hiddenQuestion(
            "What is the current Status of your suspended ASIN/Account?",
            "Have you sent any appeals to Amazon yet?"
          );

          barProgess = 70;
          if (
            TGP_statusForm3[
              "What is the current Status of your suspended ASIN/Account?"
            ] == "#1Amazon is asking for more information."
          ) {
            TGP_hiddenQuestion(
              "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
              "What is the current Status of your suspended ASIN/Account?"
            );

            barProgess = 80;
            if (
              TGP_statusForm3[
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "0"
            ) {
              TGP_Template.template1;
              TGP_hiddenQuestion(
                "Were the intellectual property complaints valid or invalid?",
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );
              barProgess = 90;
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] == "Valid"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] == "Invalid"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
            if (
              TGP_statusForm3[
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "1/3"
            ) {
              TGP_Template.template1;
              TGP_hiddenQuestion(
                "Were the intellectual property complaints valid or invalid?",
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );
              barProgess = 90;
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] == "Valid"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] == "Invalid"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
            if (
              TGP_statusForm3[
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "4+"
            ) {
              template = TGP_Template.template2;
              TGP_hiddenQuestion(
                "Have you sent any escalation letters yet?",
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );
              barProgess = 90;
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "No"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "Yes"
              ) {
                TGP_hiddenQuestion(
                  "Where did you send the escalation letter?",
                  "Have you sent any escalation letters yet?"
                );
                barProgess = 95;
                if (
                  TGP_statusForm3[
                    "Where did you send the escalation letter?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
            }
            if (
              TGP_statusForm3[
                "1How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "Default"
            ) {
            }
          } else if (
            TGP_statusForm3[
              "What is the current Status of your suspended ASIN/Account?"
            ] == "#2Amazon is not responding."
          ) {
            TGP_hiddenQuestion(
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
              "What is the current Status of your suspended ASIN/Account?"
            );

            barProgess = 80;
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "0"
            ) {
              TGP_Template.template1;
              TGP_hiddenQuestion(
                "Were the intellectual property complaints valid or invalid?",
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );

              barProgess = 90;
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] != "Default"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "1"
            ) {
              TGP_Template.template1;
              TGP_hiddenQuestion(
                "Were the intellectual property complaints valid or invalid?",
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );

              barProgess = 90;
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] != "Default"
              ) {
                disableBtn = false;
              }
            }
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "2+"
            ) {
              template = TGP_Template.template2;
              TGP_hiddenQuestion(
                "Have you sent any escalation letters yet?",
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );

              barProgess = 90;
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "No"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "Yes"
              ) {
                TGP_hiddenQuestion(
                  "Where did you send the escalation letter?",
                  "Have you sent any escalation letters yet?"
                );

                barProgess = 95;
                if (
                  TGP_statusForm3[
                    "Where did you send the escalation letter?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
            }
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "Default"
            ) {
            }
          } else if (
            TGP_statusForm3[
              "What is the current Status of your suspended ASIN/Account?"
            ] == "#3 Amazon told me their decision is final."
          ) {
            TGP_hiddenQuestion(
              "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?",
              "What is the current Status of your suspended ASIN/Account?"
            );

            barProgess = 80;
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "0"
            ) {
              TGP_Template.template1;
              TGP_hiddenQuestion(
                "Were the intellectual property complaints valid or invalid?",
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );

              barProgess = 90;
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] != "Default"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "1"
            ) {
              TGP_Template.template1;
              TGP_hiddenQuestion(
                "Were the intellectual property complaints valid or invalid?",
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );

              barProgess = 90;
              if (
                TGP_statusForm3[
                  "Were the intellectual property complaints valid or invalid?"
                ] != "Default"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
            }
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "2+"
            ) {
              template = TGP_Template.template2;
              TGP_hiddenQuestion(
                "Have you sent any escalation letters yet?",
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              );

              barProgess = 90;
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "No"
              ) {
                disableBtn = false;
                barProgess = 0;
              }
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "Yes"
              ) {
                TGP_hiddenQuestion(
                  "Where did you send the escalation letter?",
                  "Have you sent any escalation letters yet?"
                );

                barProgess = 95;
                if (
                  TGP_statusForm3[
                    "Where did you send the escalation letter?"
                  ] != "Default"
                ) {
                  disableBtn = false;
                  barProgess = 0;
                }
              }
              if (
                TGP_statusForm3["Have you sent any escalation letters yet?"] ==
                "Default"
              ) {
              }
            }
            if (
              TGP_statusForm3[
                "23How many appeal letters from Amazon Appeal Pro have you sent to Amazon through Seller Central?"
              ] == "Default"
            ) {
            }
          }
          if (
            TGP_statusForm3[
              "What is the current Status of your suspended ASIN/Account?"
            ] == "Select"
          ) {
          }
        }
        if (
          TGP_statusForm3["Have you sent any appeals to Amazon yet?"] ==
          "Default"
        ) {
        }
      }
      if (
        TGP_statusForm3[
          "Have you sent a retraction letter to the rights owner yet?"
        ] == "Default"
      ) {
      }
    }
    if (
      TGP_statusForm3["Has Amazon asked you to contact the rights owner?"] ==
      "Default"
    ) {
    }
  }

  return {
    disableBtn,
    template,
  };
};

const TGP_loadForm3 = () => {
  nameFormUse = "Intellectual Property  Suspensions";
  TGP_loadContent("Intellectual Property  Suspensions");
  TGP_validateForm = TGP_validateForm3;
  TGP_loadFunctionForm(TGP_statusForm3);
  functionValidateForm = TGP_validateForm3;
  TGP_hiddenQuestion2("Has your content been removed under DMCA?");
};
