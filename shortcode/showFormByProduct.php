<?php

function FORMBYPRODUCT_printForms() {
    $user_id = get_current_user_id();

    // Obtiene los productos comprados
    $pedidos = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_type' => wc_get_order_types(),
        'post_status' => array('wc-processing', 'wc-completed')
    ));
    $products = array();
    foreach ($pedidos as $pedido) {
        $order = wc_get_order($pedido->ID);
        foreach ($order->get_items() as $item_id => $item) {
            $product_id = $item->get_product_id();
            $product = wc_get_product($product_id);
            $template_id = get_post_meta($product_id, "template", true);
            if ($template_id) {
                $products[$product_id] = $product->get_name();
            }
        }
    }

    // Redirige al formulario correspondiente si se seleccionó un producto
    if (isset($_GET['product_id']) && array_key_exists($_GET['product_id'], $products)) {
        $product_id = $_GET['product_id'];
        $template_id = get_post_meta($product_id, "template", true);
        if ($template_id) {
            echo do_shortcode('[elementor-template id="'.$template_id.'"]');
            return;
        }
    }

    // Lista los productos comprados si no se seleccionó ninguno
    echo '<div class="content-packs">';
    foreach ($products as $product_id => $product_name) {
        $product = wc_get_product($product_id);
        echo '<div class="product">';
        echo '<div class="product-image">' . $product->get_image() . '</div>';
        echo '<div class="product-title">' . $product_name . '</div>';
        echo '<div class="product-button">';
        echo '<div class="elementor-widget-container">';
        echo '<div class="elementor-button-wrapper">';
        echo '<a href="?product_id=' . $product_id . '" class="elementor-button-link elementor-button elementor-size-sm" role="button">';
        echo '<span class="elementor-button-content-wrapper">';
        echo '<span  class="elementor-button-text">Create my Appeal Letter!</span>';
        echo '</span>';
        echo '</a>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
    echo '</div>';
}
add_shortcode( 'FORMBYPRODUCT_printForms', 'FORMBYPRODUCT_printForms' );

add_action( 'template_redirect', 'redirect_to_downloads_page_if_purchased' );
function redirect_to_downloads_page_if_purchased() {
    // Obtener el ID del producto actual
    $product_id = get_queried_object_id();

    // Verificar si el usuario ha comprado el producto
    if ( wc_customer_bought_product( wp_get_current_user()->user_email, get_current_user_id(), $product_id ) ) {
        // Redirigir a la página de descargas
        wp_redirect( home_url( '/my-account/subscriptions/' ) );
        exit;
    }
}


function FORMBYPRODUCTS_printForms() {
    $user_id = get_current_user_id();

    // Get all product IDs that the user has purchased
    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_type' => wc_get_order_types(),
        'post_status' => array('wc-processing'),
    ));
    $purchased_product_ids = array();
    foreach ($customer_orders as $customer_order) {
        $order = wc_get_order($customer_order);
        foreach ($order->get_items() as $item_id => $item) {
            $product_id = $item->get_product_id();
            $variation_id = $item->get_variation_id();
            if ($variation_id != 0) {
                $product_id = $variation_id;
            }
            $purchased_product_ids[] = $product_id;
        }
    }

    // Get all product IDs
    $all_product_ids = get_posts(array(
        'numberposts' => -1,
        'post_type' => 'product',
        'fields' => 'ids',
    ));

    // Get the product IDs that the user hasn't purchased
    $unpurchased_product_ids = array_diff($all_product_ids, $purchased_product_ids);

    // Print each unpurchased product
    $output = '<div>';
    foreach ($unpurchased_product_ids as $product_id) {
        $product = wc_get_product($product_id);
        $template_id = get_post_meta($product_id, "template", true);
        $output .= '<div>';
        $output .= $product->get_image(array(300, 300));
        $output .= '<h1>' . $product->get_title() . '</h1>';
        $output .= '<button onClick="window.location.href=\'https://amazonappeal.io/my-account/downloads/\'">Download Now</button>';
        $output .= do_shortcode('[elementor-template id="' . $template_id . '"]');
        $output .= '</div>';
    }
    $output .= '</div>';
    return $output;
}
add_shortcode( 'FORMBYPRODUCTS_printForms', 'FORMBYPRODUCTS_printForms' );

function redirect_to_form_after_purchase( $order_id ) {
    $order = wc_get_order( $order_id );
    $user_id = $order->get_user_id();
    $products = $order->get_items();
    foreach ( $products as $product ) {
        $product_id = $product->get_product_id();
        $template_id = get_post_meta( $product_id, "template", true );
        if ( $template_id ) {
            $form_url = 'https://amazonappeal.io/form-amazon/?product_id=' . $product_id;
            wp_redirect( $form_url );
            exit;
        }
    }
}
add_action( 'woocommerce_thankyou', 'redirect_to_form_after_purchase' );