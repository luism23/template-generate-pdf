<?php
/*
Plugin Name: Template Generate Pdf
Plugin URI: https://gitlab.com/luism23/template-generate-pdf
Description: Template Generate Pdf
Author: LuisMiguelNarvaez
Version: 1.1.20
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/template-generate-pdf',
	__FILE__,
	'Template Generate Pdf'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');

define("TGP_PATH",plugin_dir_path(__FILE__));
define("TGP_URL",plugin_dir_url(__FILE__));

function TGP_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}
function TGP_add_source() {
    wp_enqueue_style( 'TGP_style',TGP_URL."css/style.css", array(),  TGP_get_version(), 'all');

    wp_enqueue_script( 'TGP_loader',TGP_URL."js/loader.js", array(),  TGP_get_version(), 'all');
    wp_enqueue_script( 'TGP_functions',TGP_URL."js/functions.js", array(),  TGP_get_version(), 'all');
    wp_enqueue_script( 'TGP_template',TGP_URL."js/template.js", array(),  TGP_get_version(), 'all');
    wp_enqueue_script( 'TGP_form1',TGP_URL."js/form1.js", array(),  TGP_get_version(), 'all');
    wp_enqueue_script( 'TGP_form2',TGP_URL."js/form2.js", array(),  TGP_get_version(), 'all');
    wp_enqueue_script( 'TGP_form3',TGP_URL."js/form3.js", array(),  TGP_get_version(), 'all');
}
add_action( 'wp_enqueue_scripts', 'TGP_add_source' );


require TGP_PATH."shortcode/showFormByProduct.php";